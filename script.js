const imageContainer = document.getElementById("image-container");
const loader = document.getElementById("loader");

let ready = false;
let imagesLoaded = 0;
let totalImages = 0;
let photosArray = [];

// Unsplash API 
const count = 10;
const apikey = "xRF2L0bh5b3lJxL8LNav1jEpXWBaSxmB5fGhs9IMe2I";
const apiUrl = `https://api.unsplash.com/photos/random/?client_id=${apikey}&count=${count}`;

// Check if all images were loaded
function imageLoaded() {
    imagesLoaded++;
    if (imagesLoaded === totalImages) {
        ready = true;
        loader.hidden = true;
    }
}

// Helper function to set attributes
function setAttributes(element, attributes){
    for (const key in attributes) {
        element.setAttribute(key, attributes[key]);
    }
}

// Create the Image elements
function displayPhotos() {
    imagesLoaded = 0;
    totalImages = photosArray.length;
    // For each method to access all elements
    photosArray.forEach((photo) => {
        // Create a element
        const item = document.createElement('a');
        setAttributes(item, {
            href : photo.links.html,
            target : "_blank",
        });
        // Create image element
        const img = document.createElement("img");
        setAttributes(img, {
            src : photo.urls.regular,
            alt : photo.alt_description,
            title : photo.alt_description,
        });
        // Event listner to check when image is loaded
        img.addEventListener("load", imageLoaded);
        // Put image inside anchor element and put anchor element in container element
        item.appendChild(img);
        imageContainer.appendChild(item);
    });
}

// Get photos
async function getPhotos() {
    try {
        const response = await fetch(apiUrl);
        photosArray = await response.json();
        displayPhotos();
    }
    catch (error) {
        alert(error);
    }
}

// Check if scrolling near the bottom of page
window.addEventListener('scroll', () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight - 1000 && ready) {
        ready = false;
        getPhotos();
    }
});

// On start
getPhotos();
